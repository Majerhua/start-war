import {DataBaseRepository} from "../../../../../src/Context/Mooc/Person/Domain/DataBaseRepository";
import {Person} from "../../../../../src/Context/Mooc/Person/Domain/Person";
import {PersonName} from "../../../../../src/Context/Mooc/Person/Domain/PersonName";

export class PersonRepositoryMock implements DataBaseRepository {
  private readonly saveMock: jest.Mock
  private readonly findByNameMock: jest.Mock
  private person: Person | null

  constructor() {
    this.findByNameMock = jest.fn()
    this.saveMock = jest.fn()
  }

  async findByName(name: PersonName): Promise<Person | null> {
    this.findByNameMock(name)
    return this.person
  }


  async save(person: Person): Promise<void> {
    this.saveMock(person)
  }

  returnOnFindByName(person: Person | null) {
    this.person = person
  }

  assertSaveHaveBeenCalledWith(expected: Person): void {
    expect(this.saveMock).toHaveBeenCalledWith(expected)
  }

  assertFindByName() {
    expect(this.findByNameMock).toHaveBeenCalled()
  }
}