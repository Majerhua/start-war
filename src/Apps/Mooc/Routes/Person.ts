import {API} from "lambda-api";
import {SearchPerson} from "../Controllers/SearchPerson";
import {CreatePerson} from "../Controllers/CreatePerson";

const register = (api: API): void => {
  const searchPerson = new SearchPerson()
  const createPerson = new CreatePerson()
  api.get(
    "/",
    searchPerson.run
  )
  api.post(
    "/",
    createPerson.run
  )
}

export default register