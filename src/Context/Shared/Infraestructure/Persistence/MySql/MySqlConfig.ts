export interface MySqlConfig {
  user: string,
  host: string,
  database: string,
  password: string,
  port: number
}