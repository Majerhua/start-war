import {MySqlConfig} from "../../../../../Shared/Infraestructure/Persistence/MySql/MySqlConfig";

export class MySqlConfigFactory {
  static createConfig(): MySqlConfig {
    return {
      database: String(process.env.DATA_BASE),
      host: String(process.env.HOST),
      password: String(process.env.PASSWORD),
      port: Number(process.env.PORT),
      user: String(process.env.USER)
    }
  }
}