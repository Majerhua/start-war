import httpStatus from "http-status";

export class PersonNotFound extends Error {
  readonly status: number

  constructor(message: string) {
    super(message);
    this.status = httpStatus.NOT_FOUND
  }

}