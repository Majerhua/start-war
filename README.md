# Api start war

## Documentación

https://documenter.getpostman.com/view/16406550/2s9Y5VUjPU

## Repositorio

https://gitlab.com/Majerhua/start-war.git

# EndPoinds

1) Para crear nuevos personajes: POST  https://5owr4v14m6.execute-api.us-east-2.amazonaws.com/dev/person

```Json
{
  "nombre": "Juanito alcachofaz",
  "anio_nacimiento": "19BBY",
  "color_ojos": "blue",
  "genero": "male",
  "color_cabello": "male",
  "altura": "172",
  "masa": "77",
  "color_piel": "fair",
  "fecha_creacion": "2014-12-09T08:50:51.000Z",
  "fecha_actualizacion": "2014-12-20T16:17:56.000Z"
}
```

2) Para recuperar un personaje: GET  https://5owr4v14m6.execute-api.us-east-2.amazonaws.com/dev/person?name=luke

# Consideracion

1) El proyecto está enfocado la arquitectura hexagonal
2) Enfocado en DDD
3) La carpeta App contiene la configuración del servidor
4) La carpeta Context contiene la lógica de negocios
5) Handlers son los puntos de entrada (lambdas)
6) Los test no están cubriendo todos los casos faltan (test de integración y test de aceptación)

## Inicializar las bases de datos

```bash
docker run --name mysql -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 -d mysql:8.0.22
```

## Crear la tabla de tarjetas (Person)

```sql
CREATE TABLE person (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    birth_year VARCHAR(10),
    eye_color VARCHAR(50),
    gender VARCHAR(20),
    hair_color VARCHAR(50),
    height VARCHAR(10),
    mass VARCHAR(10),
    skin_color VARCHAR(50),
    created DATETIME,
    edited DATETIME
);
```

## Build project

```bash
npm run build
```

## test

```bash
npm run test
```

## Crear variables de entorno

Crea un archivo env.dev.json y copia los valores que hay en el archivo de example.dev.json

## Ejecutar entorno local

1) Se tiene que tener serverless-offline para instalar: npm i -g serverless-offline
2) Ejecutar: npm run start:local

## Despliegue a AWS

Ejecutar: npm run deploy:dev
